/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This class is accountable of the content of Menu, that is showed when opening the program
 */
public class Menu extends Application{
    //some constants
    public static final int MOVIE_BY_ID=7;
    public static final int MOVIES_BY_PROFESSIONAL_ID=9;
    public static final int PROFESSIONALS_BY_MOVIE_ID=6;
    //using singleton pattern
    public static final Menu INSTANCE=new Menu();
    //constructor is private when using the singleton pattern
    private Menu(){};
    private Connection connection;

    private int searchBy=MOVIE_BY_ID;
    private ArrayList<Movie> movies;


    /**
     * the 'constructor' method in the singleton pattern
     * @return the only instance of Menu class
     */
    public static Menu getInstance(){
        return INSTANCE;
    }

    /**
     * gets connection
     * @return connection
     */
    public Connection getConnection(){
        return connection;
    }

    /**
     * sets the connection
     * @param con connection to set to
     */
    public void setConnection(Connection con){
        connection=con;
    }

    /**
     * sets type of serach
     * @param sb type of search
     */
    public void setSearchBy (int sb){
        searchBy = sb;
    }
    public int getSearchBy() {
        return searchBy;
    }

    /**
     * returns the movies that are updated server
     * @return array of movies
     */
    public ArrayList<Movie> getMovies() { return movies;   }

    /**
     * sets the movies array
     * @param movie movies to set to
     */
    public void setMovies(ArrayList<Movie> movie ) {movies=movie;}

    /**
     * updates the movies from server.
     */
    public void updateMovies(){
        movies=new ArrayList<Movie>();
        String moviesString="";
        try{
            //request movies from server
           moviesString= connection.sendAndRecive("13");
            if(moviesString.equals("empty\n")|| moviesString.equals("Failure\n")){
                //no movies
                return;
            }

            String []temp=moviesString.split("}");

            for(int i=0;i<temp.length;i++){
                //qdds movies from server to member of Menu
                if(temp[i].equals("\n"))
                    continue;
                movies.add(new Movie(temp[i]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * updates movies
     * @param message masseage to send to server
     */
    public void updateMovies(String message){
        movies=new ArrayList<Movie>();
        String moviesString="";
        try{
            moviesString= connection.sendAndRecive(message);
            if(moviesString.equals("empty\n")|| moviesString.equals("Failure\n"))
                //no movies
                return;
            String []temp=moviesString.split("}");

            for(int i=0;i<temp.length;i++){
                if(temp[i].equals("\n"))
                    continue;
                movies.add(new Movie(temp[i]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * gets array of professionals from server
     * @return array of pro's
     */
    public ArrayList<String> getProfessionalList(){
        String temp="";
        ArrayList<String> al =new ArrayList<String>();
        try{
            //requestion
            temp=connection.sendAndRecive("14");
            if(temp.equals("empty\n")||temp.equals("Failure\n"))
                //no pro's to send
                return al;
        }catch (IOException e){
            e.printStackTrace();
        }
        if(temp.charAt(0)=='\n')
            temp=temp.substring(1);
        String[] tempB=temp.split("\n");
        String [] tempC;
        for(int i=0;i<tempB.length;i++){
            tempC=tempB[i].split("]");
            tempB[i]= tempC[0];
            if(tempC.length>1)
                tempB[i]+=tempC[1];
            al.add(tempB[i]);
        }
        return al;
    }

    /**
     * * gets array of professionals from server
     * @param message to send to server
     * @return array of pro's
     */
    public ArrayList<String> getProfessionalList(String message){
        String temp="";
        ArrayList<String> al =new ArrayList<String>();
        try{
            temp=connection.sendAndRecive(message);
            if(temp.equals("empty\n")||temp.equals("Failure\n"))
                //no pro's to send
                return al;
        }catch (IOException e){
            e.printStackTrace();
        }
        if(temp.charAt(0)=='\n')
            temp=temp.substring(1);
        String[] tempB=temp.split("\n");
        String [] tempC;
        for(int i=0;i<tempB.length;i++){
            tempC=tempB[i].split("]");
            tempB[i]= tempC[0];
            if(tempC.length>1)
                tempB[i]+=tempC[1];
            al.add(tempB[i]);
        }
        return al;
    }

    /**
     * shows the menu on secrrn
     * @param primaryStage stage to show in
     * @throws Exception if can't show
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        primaryStage.setTitle("Movies");
        Scene scene = new Scene(root, 900, 600);
        scene.getStylesheets().add(getClass().getResource("menu.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
