/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ListCell;

import java.io.IOException;

/**
 * This class is accountable of loading the updated movies list
 */
public class MyListRow extends ListCell<Movie> {

    @Override
    protected void updateItem(Movie movie,boolean empty){
        super.updateItem(movie,empty);
        if(movie!=null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MovieRow.fxml"));
            MovieRowControler controler = new MovieRowControler(movie);
            loader.setController(controler);

            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e){
                e.printStackTrace();
            }
          //  ImageView a= new ImageView();
        //    setGraphic(a);
            setGraphic(root);

        }

    }
}