/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * AddMovie class.
 * This class is accountable of showing the Add Proe window.
 */
public class AddProfessional {
    /**
     * the functions shows the add pro window
     * @throws IOException
     */
    public void show() throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("AddProfessional.fxml"));
        stage.setTitle("Add Professional");
        Scene scene = new Scene(root, 550, 550);
        scene.getStylesheets().add(getClass().getResource("menu.css").toExternalForm());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
