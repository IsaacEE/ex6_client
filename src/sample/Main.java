/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.application.Application;
import javafx.stage.Stage;


/**
 * This main class.
 */
public class Main extends Application {
//test again

    @Override
    public void start(Stage primaryStage) throws Exception{
        Menu menu=Menu.getInstance();
        try {
            menu.start(primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch();
    }
}
