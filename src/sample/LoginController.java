/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * This class is accountable of controlling the login window, managing its content and events
 */
public class LoginController {

    @FXML
    Pane window;
    @FXML
    Button btnLogin;

    //should let them id

   @FXML
   Label login;

   @FXML
   VBox vBox;
   @FXML
   TextField ipField;

   @FXML
   TextField portField;

    @FXML
    void initialize(){
        String image = getClass().getResource("images/winBG.jpg").toExternalForm();
        window.setStyle("-fx-background-image: url('" + image + "');");

        Menu menu=Menu.getInstance();
        if(menu.getConnection()!=null) {
            //if not first entrance, load values
            ipField.setText(menu.getConnection().getIp());
            portField.setText(menu.getConnection().getPort());
        }

        btnLogin.setOnMouseClicked( new EventHandler<MouseEvent>() {
            /**
             * the function handles an event.
             * @param event the event that the function was called for
             */
            @Override
            public void handle(MouseEvent event) {
                //if client pressed "login"
                Menu menu= Menu.getInstance();
                try {
                    Connection connection= new Connection(Integer.parseInt(portField.getText()),ipField.getText());
                    menu.setConnection(connection);
                    connection.connect();
                    //closes login window
                    Stage stage = (Stage) btnLogin.getScene().getWindow();
                    stage.close();
                }
                catch (Exception e){
                    System.out.println("Error connecting");
                }


            }
        });

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
            }
        });
    }


}
