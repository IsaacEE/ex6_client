/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * RemoveMovie class.
 * This class is accountable of showing the 'Remove Movie' window.
 */
public class RemoveMovie {
    /**
     * shos the 'Remove movie' winodow
     * @throws IOException if can't show
     */
    public void show() throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("RemoveMovie.fxml"));
        stage.setTitle("Remove Movie");
        Scene scene = new Scene(root, 450, 200);
        scene.getStylesheets().add(getClass().getResource("menu.css").toExternalForm());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
