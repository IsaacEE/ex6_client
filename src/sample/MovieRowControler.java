/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * This class is accountable of managing list of movies that would show up
 * when the client will press the all button
 */

public class MovieRowControler {
   @FXML
    Text codeNameYear;
    @FXML
    Text lengthGenre;
    @FXML
    Text rate;
    @FXML
    Text description;
    @FXML
    Text actors;
    @FXML
    ImageView movieImage;
    private Movie movie;

    //constructor
    public MovieRowControler(Movie m){
        movie=m;
    }
    @FXML
    public void initialize(){
        codeNameYear.setText("#"+  movie.getCode()+" - "+movie.getName()+"("+movie.getYear()+")  ");
        String genre="";
        boolean first = true;
        if(movie.getGenre()!=null){
            for (int i=0; i<movie.getGenre().length;i++){
                if(!first)
                    genre+=" | "+movie.getGenre()[i];
                else{
                    genre+=movie.getGenre()[i];
                    first=false;
                }
            }
        }


        lengthGenre.setText(""+movie.getLength()+" min"+" - "+genre);
        rate.setText("Rate: "+movie.getRating()+"/10");
        description.setText(movie.getSummery());
        String act="";
        if(!movie.getProfessionals().isEmpty())
            act=movie.getProfessionals().toString().substring(1,movie.getProfessionals().toString().length()-1);
        actors.setText("Actors:    "+act);
        Image tempImage;
        try {
            tempImage =new Image(movie.getImageLink());
        }catch (Exception e){
            tempImage =new Image("sample/images/move.jpg");
        }
        movieImage.setImage(tempImage);
    }
}
