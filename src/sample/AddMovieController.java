/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.text.NumberFormat;

/**
 * This class is accountable of controlling the add movie window, managing its content and events
 */
public class AddMovieController {
    @FXML
    Pane window;
    @FXML
    TextField idField;
    @FXML
    TextField nameField;
    @FXML
    TextField lengthField;
    @FXML
    TextField yearField;
    @FXML
    TextField ratingField;
    @FXML
    TextField urlField;
    @FXML
    TextField descField;
    @FXML
    TextField genreField;
    @FXML
    Slider slider;
    @FXML
    Button addBtn;
    //default value of rating
    private static final double INIT_VAL=5;

    @FXML
    void initialize() throws IOException {
        String image = getClass().getResource("images/winBG.jpg").toExternalForm();
        window.setStyle("-fx-background-image: url('" + image + "');");


        //initializing slider and rating field, binding them one to the other
        slider.setValue(INIT_VAL);
        ratingField.setText(Double.toString(INIT_VAL));
        ratingField.textProperty().bindBidirectional(slider.valueProperty(), NumberFormat.getNumberInstance());

        Menu menu = Menu.getInstance();
        addBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            /**
             * the function handles an event.
             * @param event the event that the function was called for
             */
            @Override
            public void handle(MouseEvent event) {
                String toSend ="1 "+idField.getText()+" "+ nameField.getText()+" " + lengthField.getText() +
                        " "+ yearField.getText()+" "+ratingField.getText()+" "+urlField.getText()+" "+descField.getText()+"\n";
                try {
                    //send client's input in fields to server
                    String receive = menu.getConnection().sendAndRecive(toSend);

                    if(!genreField.getText().equals("")){
                        //if client added genres
                        String[] genres=genreField.getText().split(",");
                        for(int i=0; i<genres.length; i++){
                            //sends genre one by one
                            String newGenreSend="4"+" "+idField.getText()+" "+genres[i];
                            receive = menu.getConnection().sendAndRecive(newGenreSend);
                        }
                        genreField.setText("");
                    }
                    if(receive!=null && receive.equals("Success\n")) {
                        //nullifies text fields
                        idField.setText("");
                        lengthField.setText("");
                        yearField.setText("");
                        urlField.setText("");
                        descField.setText("");
                        nameField.setText("");
                        //initializing slider and rating field, binding them one to the other
                        slider.setValue(INIT_VAL);
                        ratingField.setText(Double.toString(INIT_VAL));
                        ratingField.textProperty().bindBidirectional(slider.valueProperty(), NumberFormat.getNumberInstance());
                    }
                } catch (IOException e) {
                    System.out.println("Error sending message");
                }
            }
        });


    }
}
