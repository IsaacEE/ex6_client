/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * This class is accountable of controlling the add pro window, managing its content and events
 */
public class AddProfessionalController {
    @FXML
    Pane window;
    @FXML
    TextField typeField;
    @FXML
    TextField idField;
    @FXML
    TextField ageField;
    @FXML
    TextField descField;
    @FXML
    TextField nameField;
    @FXML
    TextField moviesField;
    @FXML
    RadioButton actor;
    @FXML
    RadioButton director;
    @FXML
    RadioButton screenWriter;
    @FXML
    RadioButton producer;
    @FXML
    ToggleButton male;
    @FXML
    ToggleButton female;
    @FXML
    Button addBtn;

    @FXML
    void initialize() throws IOException {
        //sets background
        String image = getClass().getResource("images/winBG.jpg").toExternalForm();
        window.setStyle("-fx-background-image: url('" + image + "');");

        Menu menu = Menu.getInstance();
        addBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            /**
             * the function handles an event.
             * @param event the event that the function was called for
             */
            @Override
            public void handle(MouseEvent event) {
                //gets client's choise from radio button
                String type="";
                //gets type of pro from text field and converts it to servers language
                if(director.isSelected()) type="0";
                else if(actor.isSelected())type="1";
                else if(screenWriter.isSelected())type="2";
                else if(producer.isSelected())type="3";
                //gets client's choise from toggle button (gender)
                String gender="";
                if(male.isSelected()) gender="male";
                else if(female.isSelected()) gender="female";
                //message to send
                String toSend ="2 "+type+" "+ idField.getText()+" " + ageField.getText() +
                        " "+ descField.getText()+" "+gender+" "+nameField.getText()+"\n";
                try {
                    //send message
                    String receive = menu.getConnection().sendAndRecive(toSend);

                    if(!moviesField.getText().equals("")){
                        //if client added genres adds genres to pro
                        String[] movies=moviesField.getText().split(",");
                        for(int i=0; i<movies.length; i++){
                            //send genre to add
                            String newGenreSend="3"+" "+movies[i]+" "+idField.getText();
                            receive = menu.getConnection().sendAndRecive(newGenreSend);
                        }
                        moviesField.setText("");
                    }
                    if(receive!=null) {
                        //nullifies text fields
                        idField.setText("");
                        ageField.setText("");
                        descField.setText("");
                        nameField.setText("");
                        male.setSelected(true);
                        actor.setSelected(true);
                    }
                } catch (IOException e) {
                    System.out.println("Error sending message");
                }

            }
        });

    }
}
