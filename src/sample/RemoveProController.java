/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;

/**
 * RemoveProController  class.
 * This class is accountable of managing the 'Remove pro' class (window and content))
 */
public class RemoveProController {

    @FXML
    TextField idField;
    @FXML
    Button rmvBtn;

    /**
     * initializes window and context
     * @throws IOException if unable
     */
    @FXML
    void initialize() throws IOException {

        Menu menu = Menu.getInstance();
        String redCol="-fx-background-color: #FF6347";
        rmvBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            /**
             * handle's user request to erase a pro
             * @param event the mouse click on remove
             */
            @Override
            public void handle(MouseEvent event) {
                String toSend ="11 "+idField.getText();
                try {
                    String receive = menu.getConnection().sendAndRecive(toSend);

                    if(receive.equals("Success\n")) {
                        idField.setText("");
                    }
                    else {
                        //make field red  as sign to failure
                        idField.setStyle(redCol);
                    }

                } catch (IOException e) {
                    System.out.println("Error sending message");
                }
            }


        });
        idField.setOnKeyTyped(new EventHandler<KeyEvent>() {
            /**
             * suppose to be called when we want a red text field get to normal color
             * @param event when user touch's keyboard, the red field (if it is red) will be normal again
             */
            @Override
            public void handle(KeyEvent event) {
                if(idField.getStyle().equals(redCol))
                    //if field is red
                    idField.setStyle("");
            }
        });

    }



}
