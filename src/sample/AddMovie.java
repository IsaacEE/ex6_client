/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * AddMovie class.
 * This class is accountable of showing the Add Movie window.
 */
public class AddMovie {

    /**
     * the functions shows the add movie window
     * @throws IOException
     */
    public void show() throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("AddMovie.fxml"));
        stage.setTitle("Add Movie");
        Scene scene = new Scene(root, 550, 550);
        scene.getStylesheets().add(getClass().getResource("menu.css").toExternalForm());
        //makes other windows unreachable while this window is opened
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
