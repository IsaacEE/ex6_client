/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import java.util.ArrayList;

/**
 * This class is accountable of managing the string (the server sends) and parses it to
 * movie's value, so it can be showed up.
 */
public class Movie {

    private String code;
    private String name;
    private String imageLink;
    private int length;
    private int year;
    private float rating;
    private String summery;
    String[] genre;
    ArrayList<String> professionals;

    /**
     * gets movie's code
     * @return code
     */
    public String getCode() {return code;}
    /**
     * gets movie's rating
     * @return rating
     */
    public float getRating() {
        return rating;
    }
    /**
     * gets movie's length
     * @return length
     */
    public int getLength() {
        return length;
    }
    /**
     * gets movie's pro's
     * @return pro's array
     */
    public ArrayList<String> getProfessionals() {
        return professionals;
    }

    /**
     * gets movie's year
     * @return year
     */
    public int getYear() {
        return year;
    }
    /**
     * gets movie's image
     * @return image
     */
    public String getImageLink() {
        return imageLink;
    }
    /**
     * gets movie's name
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * gets movie's summary
     * @return summary
     */
    public String getSummery() {
        return summery;
    }
    /**
     * gets movie's genres
     * @return genres
     */
    public String[] getGenre() {
        return genre;
    }
    //constructor
    public Movie (String movieString){
        professionals= new ArrayList<String>();
        if(movieString.charAt(0)=='\n')
            movieString=movieString.substring(1);
        String[] movieLines = movieString.split("\n");
        //prases from string
        String[] moviefields = movieLines[0].split(" ");
        code = moviefields[0];
        name = moviefields[1];
        length = Integer.parseInt(moviefields[2]);
        year = Integer.parseInt(moviefields[3]);
        rating = Float.parseFloat(moviefields[4]);
        if(moviefields[5].equals("noGenre"))
            //if now genres
            genre=null;
        else
            genre=moviefields[5].split(",");
        imageLink=moviefields[6];
        int i;
        summery= "";
        for(i= 7;i<moviefields.length;i++){
            summery+=moviefields[i]+" ";
        }

        for(i=1;i<movieLines.length;i++){
            //gets the name
            professionals.add(movieLines[i].split("]")[0]);
        }


    }
}
