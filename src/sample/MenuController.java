/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.io.IOException;
/**
 * This class is accountable of controlling the menu window, managing its content and events
 */
public class MenuController {
    @FXML
    BorderPane board;
    @FXML
    Button logo;
    @FXML
    MenuItem addMovie;
    @FXML
    MenuItem addProfessional;
    @FXML
    MenuItem removeMovie;
    @FXML
    MenuItem removeProfessional;
    @FXML
    Button search;
    @FXML
    MenuButton searchBy;
    @FXML
    MenuItem movieByProfessional;
    @FXML
    MenuItem professionalByMovie;
    @FXML
    MenuItem movieById;
    @FXML
    TextField searchField;
    @FXML
    ListView moviesListView;
    @FXML
    ListView movieListView;
    @FXML
    Button showAll;
    @FXML
    MenuItem showProfessionals;
    @FXML
    MenuItem showMovies;
    @FXML
    ListView professionalListView;
    @FXML
    ImageView aaa;

    @FXML
    void initialize() throws IOException {
        //setting background
        String image = getClass().getResource("images/bckgrnd.png").toExternalForm();
        board.setStyle("-fx-background-image: url('" + image + "');");

        showMovies.setOnAction(new EventHandler<ActionEvent>(){
            /**
             * the function handles an event, in this case, pressed on show movies
             * @param event the event that the function was called for
             */
            @Override
            public void handle(ActionEvent event) {
                //updates movies from movies list
                Menu.getInstance().updateMovies();
                movieListView=new ListView();
                board.setCenter(movieListView);

                movieListView.setCellFactory(new Callback<ListView, ListCell>() {
                    @Override
                    public ListCell call(ListView param) {
                        return new MyListRow();
                    }
                });
                movieListView.getItems().clear();
                movieListView.getItems().addAll(Menu.getInstance().getMovies());


            }
        });

        showProfessionals.setOnAction(new EventHandler<ActionEvent>(){
            /**
             * the function handles an event, in this case, pressed on show professionals
             * @param event the event that the function was called for
             */
            @Override
            public void handle(ActionEvent event) {
                professionalListView.getItems().clear();
                professionalListView.getItems().addAll(Menu.getInstance().getProfessionalList());

            }
        });

        search.setOnMouseClicked(new EventHandler<MouseEvent>() {
            /**
             * the function handles an event, in this case, pressed on find movie or professional
             * @param event the event that the function was called for
             */
            @Override
            public void handle(MouseEvent event) {
                Menu menu = Menu.getInstance();
                String result = "";
                if(!searchField.getText().equals("")) {
                    //if client wrote content in field
                    String messageToServer = "" + menu.getSearchBy() + " " + searchField.getText();

                        if(messageToServer.charAt(0)=='6')
                        {
                            //display professionals
                            professionalListView.getItems().clear();
                            professionalListView.getItems().addAll(Menu.getInstance().getProfessionalList(messageToServer));
                        }else {
                            //display movies
                            Menu.getInstance().updateMovies(messageToServer);
                            movieListView = new ListView();
                            board.setCenter(movieListView);

                            movieListView.setCellFactory(new Callback<ListView, ListCell>() {
                                @Override
                                public ListCell call(ListView param) {
                                    return new MyListRow();
                                }
                            });
                            movieListView.getItems().clear();
                            movieListView.getItems().addAll(Menu.getInstance().getMovies());
                        }
                    //set prompt back
                    searchField.setText("");
                }
            }
        });


        movieById.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to print by movie's id
             * @param event mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                //shows movies by id
                Menu.getInstance().setSearchBy(Menu.MOVIE_BY_ID);
                searchBy.setText("Movie by id");
            }

        });
        professionalByMovie.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to print pro's by movies
             * @param event mouse click
             */
            @Override
            //shows professional by movies
            public void handle(ActionEvent event) {
                Menu.getInstance().setSearchBy(Menu.PROFESSIONALS_BY_MOVIE_ID);
                searchBy.setText("Professionals by movie");
            }
        });
        movieByProfessional.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to print pro's by movies
             * @param event mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                //shows movies by professional
                Menu.getInstance().setSearchBy(Menu.MOVIES_BY_PROFESSIONAL_ID);
                searchBy.setText("Movies by professional id");
            }
        });


        logo.setOnMouseClicked(new EventHandler<MouseEvent>() {
            /**
             * handles client's request to get to login window
             * @param event the mouse click
             */
            @Override
            public void handle(MouseEvent event) {
                Login con = new Login();
                try {
                    con.show();
                } catch (IOException e) {
                    System.out.println("Error clicking logo");
                }
            }

        });

        addMovie.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to get to the 'add movie' window
             * @param event the mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                AddMovie addMovie = new AddMovie();
                try {
                    addMovie.show();
                } catch (IOException e) {
                    System.out.println("Error adding movie");
                }
            }
        });
        addProfessional.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to get to the 'add pro' window
             * @param event the mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                AddProfessional addProfessional = new AddProfessional();
                try {
                    addProfessional.show();
                } catch (IOException e) {
                    System.out.println("Error adding Pro");
                }
                ;
            }
        });
        removeMovie.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to get to the 'Remove movie' window
             * @param event the mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                RemoveMovie removeMovie = new RemoveMovie();
                try {
                    removeMovie.show();
                } catch (IOException e) {
                    System.out.println("Error removing movie");
                }
            }
        });
        removeProfessional.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * handles client's request to get to the 'Remove Pro' window
             * @param event the mouse click
             */
            @Override
            public void handle(ActionEvent event) {
                RemovePro removePro = new RemovePro();
                try {
                    removePro.show();
                } catch (IOException e) {
                    System.out.println("Error removing pro");
                }
            }

        });

    }
}

