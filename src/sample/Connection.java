/*8921004 305684789 Liron Matoki*/
/*8921005 301882338 Isaac El Eini*/
package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This class is accountable of the connection between the client and the server.
 */
public class Connection {

    private int port=-1;
    private String ip="-1";
    Socket socket;
    BufferedReader server;
    PrintWriter out;
    BufferedReader stdIn;

    //constructor
    public Connection(int p,String i){
        this.port=p;
        this.ip=i;
    }

    /**
     * initializes connection between client to server
     * @throws Exception if unable to connect
     */
    void connect() throws Exception{
            if(socket!=null)
                socket.close();
            socket = new Socket(ip, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            server = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            stdIn = new BufferedReader(new InputStreamReader(System.in));
    }
    /**
     * gets the port
     * @return the port as a string
     */
    public String getPort(){
        return new Integer(port).toString();
    }

    /**
     * gets ip
     * @return the ip
     */
    public String getIp(){
        return ip;
    }

    /**
     * sends and receive masege from the server
     * @param fromUser the massege to the server
     * @return the out put from the server
     * @throws IOException if problem happens during reading
     */

    public String sendAndRecive(String fromUser) throws IOException{
        String temp="";

        StringBuilder fromServer= new StringBuilder();
        //sends the message
        out.println(fromUser);

        try {
        while (!temp.equals("endLine")) {
            fromServer.append(temp);
            temp = server.readLine();
            if(!fromServer.toString().equals(""))
                fromServer.append("\n");
        }
    }
    catch(Exception e){
        System.out.println("Error, closing socket");
        socket.close();
    }
        return fromServer.toString();
    }
}
